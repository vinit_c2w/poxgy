package com.c2w_javafx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class screen extends Application {

    public void start(Stage prStage) {                                      //create the stage 
         Font ft=Font.font("Courier New",FontWeight.BOLD,30);        
        prStage.setTitle("PROXY");       // Title Of The Stage
        prStage.setHeight(500);          //Set Hight of The Stage
        prStage.setWidth(700);            //Set Width of the Stage
        prStage.setResizable(true);     // sets the resizable property of the stage

                     
        Circle JI_CP=new Circle(200);

        Image JI_image2 = new Image("Asstes/file.png");     //Create The Image
        ImageView JI_view2 = new ImageView(JI_image2);          // To Show The Image On Stage
        JI_view2.setPreserveRatio(true);                   //Set The Resizable Property Of The Image

        JI_view2.setFitHeight(70);                          //Set The Hight Of The Image
        JI_view2.setFitWidth(70);                           //Set The Width Of The Image

        JI_view2.setOnMouseClicked(event -> {                    //On Click Event After Click The Image
            System.out.println("Create The Class Room");
        });

        Button JIaddButton=new Button();
        JIaddButton.setShape(JI_CP);
        JIaddButton.setGraphic(JI_view2);


 
 DropShadow JI_shadowadd = new DropShadow();
        JIaddButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            JIaddButton.setEffect( JI_shadowadd);
        }
});
JIaddButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
    @Override public void handle(MouseEvent e) {
        JIaddButton.setEffect(null);
    }
});

        Line line = new Line();                                 //Create The Horizontal Line On Stage
        line.setStartX(0);                                 //  Starting X Coordinate of Line 
        line.setStartY(0);                                 //  Starting Y Coordinate of Line                  
        line.endXProperty().bind(prStage.widthProperty());        //End X Coordinate of Line and Width
        line.setEndY(0); 
        line.setStrokeWidth(3);                                    //End X Coordinate of Line
        

        Text tx1 = new Text("Your Classes");                // Headline Text
        tx1.setFont(ft);                          // Font Size Of Text

        VBox vb = new VBox();                                  // Create Virtual Box
        vb.setAlignment(Pos.TOP_CENTER);                       //Set Alignment Center On Screen
        vb.getChildren().addAll(tx1, line);                     //Pass The Text and Line In Virtual Box
        vb.setPadding(new Insets(30,0,0,-15));     // Set Padding of Virtual Box

        
        BorderPane root = new BorderPane();
      //  root.setTop(topRightBox);
        root.setCenter(vb);

        StackPane buttom = new StackPane(JIaddButton);
        buttom.setAlignment(Pos.BOTTOM_RIGHT);
        buttom.setPadding(new Insets(50));
        root.setBottom(buttom); // Set the bottom of the BorderPane to the StackPane
        root.setPadding(new Insets(5));

        
    

       
        Scene scene = new Scene(root,500,200);
        

        prStage.setScene(scene);
        prStage.show();
    }
}