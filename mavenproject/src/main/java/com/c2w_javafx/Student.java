package com.c2w_javafx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Student extends Application{
    public void start(Stage prStage) {
        Font ft=Font.font("Courier New",FontWeight.BOLD,30);
        prStage.setTitle("Assignment");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setResizable(true); 

        Line l=new Line();
        l.setStartX(0);
        l.setStartY(70);
        l.setEndX(1500);
        l.setEndY(70);
        l.setStrokeWidth(3);

        Text tx1 = new Text("Student");                
        tx1.setFont(ft);  

        VBox vb=new VBox();
        
         vb. getChildren().addAll(tx1,l);
         vb.setAlignment(Pos.TOP_CENTER);
        vb.setPadding(new Insets(50,0,0,1400));

        Button studymatrialBotton=new Button("Study Material");
        studymatrialBotton.setLayoutX(100);
        studymatrialBotton.setLayoutY(900);
        studymatrialBotton.setFont(ft);
         studymatrialBotton.setStyle("-fx-background-color: #194a7a; -fx-text-fill: #FFFFFF;");


         DropShadow JI_shadowforStm = new DropShadow();
        studymatrialBotton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studymatrialBotton.setEffect(JI_shadowforStm);
        }
});
studymatrialBotton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        studymatrialBotton.setEffect(null);
    }
});

        Button assginmentButton=new Button("Assignment");
        assginmentButton.setLayoutX(400);
        assginmentButton.setLayoutY(900);
        assginmentButton.setFont(ft);
        assginmentButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforAssi = new DropShadow();
        assginmentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            assginmentButton.setEffect(JI_shadowforAssi);
        }
});
assginmentButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        assginmentButton.setEffect(null);
    }
});

        Button studeButton=new Button("Student");
        studeButton.setLayoutX(650);
        studeButton.setLayoutY(900);
        studeButton.setFont(ft);
        studeButton.setStyle("-fx-background-color:#6f92b0 ; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforstud = new DropShadow();
        studeButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studeButton.setEffect(JI_shadowforstud);
        }
});
studeButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        studeButton.setEffect(null);
    }
});

        Button messButton=new Button("Message");
        messButton.setLayoutX(900);
        messButton.setLayoutY(900);
        messButton.setFont(ft);

        messButton.setStyle("-fx-background-color:#8099b0 ; -fx-text-fill: #FFFFFF;");

        DropShadow shadow = new DropShadow();
        messButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            messButton.setEffect(shadow);
        }
});
messButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        messButton.setEffect(null);
    }
});

        HBox bottBox=new HBox(30,studymatrialBotton,assginmentButton, studeButton,messButton);
        bottBox.setAlignment(Pos.CENTER);
        bottBox.setPadding(new Insets(40));

        BorderPane root=new BorderPane();
        root.getChildren().addAll(vb);
        root.setBottom(bottBox);

        Image backgroundImage = new Image("Asstes/colorkit (4).png");
        BackgroundImage background = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.DEFAULT,
            BackgroundSize.DEFAULT
        );
        root.setBackground(new Background(background));


        StackPane JI_sp=new StackPane();
        JI_sp.getChildren().addAll(root);


        

             




        Scene sc=new Scene(JI_sp,500,700);
        prStage.setScene(sc);        
        prStage.show();
     }
    
}
