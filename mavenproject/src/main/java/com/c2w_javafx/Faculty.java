package com.c2w_javafx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Faculty extends Application {

    @Override
    public void start(Stage prStage) {
        Font ft=Font.font("Courier New",FontWeight.BOLD,30);
        prStage.setTitle("In class Faculty");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setResizable(true); 

        Line l=new Line();
        l.setStartX(0);
        l.setStartY(90);
        l.setEndX(1500);
        l.setEndY(90);
        l.setStroke(Color.BLACK); 
        l.setStrokeWidth(3);
 
        HBox hb=new HBox(l);
        hb.setAlignment(Pos.CENTER);
        hb.setPadding(new Insets(100,0,0,0)); //set line padding
        
        Button studymatrialBotton=new Button("Study Material");
        studymatrialBotton.setLayoutX(100);
        studymatrialBotton.setLayoutY(900);
        studymatrialBotton.setFont(ft);
        studymatrialBotton.setStyle("-fx-background-color: #194a7a; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforStm = new DropShadow();
        studymatrialBotton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studymatrialBotton.setEffect(JI_shadowforStm);
        }
});
studymatrialBotton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        studymatrialBotton.setEffect(null);
    }
});
        Button assginmentButton=new Button("Assignment");
        assginmentButton.setLayoutX(400);
        assginmentButton.setLayoutY(900);
        assginmentButton.setFont(ft);
       assginmentButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforAssi = new DropShadow();
        assginmentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            assginmentButton.setEffect(JI_shadowforAssi);
        }
});
assginmentButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        assginmentButton.setEffect(null);
    }
});
        Button studeButton=new Button("Student");
        studeButton.setLayoutX(650);
        studeButton.setLayoutY(900);
        studeButton.setFont(ft);
        studeButton.setStyle("-fx-background-color:#7593af ; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforstud = new DropShadow();
        studeButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studeButton.setEffect(JI_shadowforstud);
        }
});
studeButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        studeButton.setEffect(null);
    }
});
        Button messButton=new Button("Message");
        messButton.setLayoutX(900);
        messButton.setLayoutY(900);
        messButton.setFont(ft);
        messButton.setStyle("-fx-background-color:#a3b7ca ; -fx-text-fill: #FFFFFF;");
  
        DropShadow shadow = new DropShadow();
        messButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            messButton.setEffect(shadow);
        }
});
messButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
new EventHandler<MouseEvent>() {
     public void handle(MouseEvent e) {
        messButton.setEffect(null);
    }
});
        HBox bottBox=new HBox(30,studymatrialBotton,assginmentButton, studeButton,messButton);
        bottBox.setAlignment(Pos.CENTER);
        bottBox.setPadding(new Insets(40));

      BorderPane root=new BorderPane();

      Circle JI_c=new Circle(200);
      
      JI_c.setCenterX(100);
      JI_c.setCenterY(100);
      JI_c.setStroke(Color.BLACK);

      Button JI_Fb = new Button("A");
      JI_Fb.setShape(JI_c);
      JI_Fb.setLayoutX(1100);
      JI_Fb.setLayoutY(21);
      JI_Fb.setMaxHeight(100);
      JI_Fb.setMinSize(40, 40);
      JI_Fb.setFont(ft);

      DropShadow JI_ShadowforA = new DropShadow();
      JI_Fb.addEventHandler(MouseEvent.MOUSE_ENTERED, 
  new EventHandler<MouseEvent>() {
       public void handle(MouseEvent e) {
          JI_Fb.setEffect(JI_ShadowforA);
      }
});

      Circle JI_c2=new Circle(200);
      JI_c2.setCenterX(100);
      JI_c2.setCenterY(100);
      
      JI_c2.setStroke(Color.BLACK);
      
      Button JI_i = new Button("i");
      JI_i.setShape(JI_c2);
      JI_i.setLayoutX(1250);
      JI_i.setLayoutY(20);
      JI_i.setMinSize(40,40);
      JI_i.setFont(ft);


      Pane JI_p2=new Pane();
      JI_p2.setLayoutX(900);
      JI_p2.setLayoutY(100);
      JI_p2.getChildren().addAll(JI_Fb,JI_i);

        root.setBottom(bottBox);
        root.setTop(hb);


        
        
        StackPane JI_SP=new StackPane();
        JI_SP.getChildren().addAll(JI_p2,root);
        JI_SP.setAlignment(Pos.TOP_RIGHT);

        Scene scene = new Scene(JI_SP,500,700);
        prStage.setScene(scene);
        prStage.show();
    }
}