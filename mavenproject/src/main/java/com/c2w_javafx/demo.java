package com.c2w_javafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class demo extends Application {

    
   

    @Override
    // Creating Stage
    public void start(Stage sc_7Stage) {
        Font ft = Font.font("Courier New", FontWeight.BOLD, 30);
        sc_7Stage.setTitle("Screen8"); // Title Of Stage
        sc_7Stage.setHeight(500); // Height Of Stage
        sc_7Stage.setWidth(700); // Width Of Stage
        sc_7Stage.setResizable(true); // Resizing Stage

        // Create a Pane to hold all the components
        Pane JI_pane = new Pane();

        Line li = new Line();
        li.setStartX(0);
        li.setStartY(100); // Distance from the top of the screen
        li.setEndX(2000); // Length of the line
        li.setEndY(100);
        li.setStrokeWidth(3);
        li.setStyle("-fx-background-color: #d1dbe4;"); 

        // Add the line to the pane
        JI_pane.getChildren().add(li);

        // Create a Label
        Label label1 = new Label("Study Material");
        label1.setPadding(new Insets(25, 300, 0, 860));
        label1.setFont(ft);
        label1.setTextFill(Color.BLACK); 

         Circle JI_arrow = new Circle(250);

        Label label2 = new Label("Add Your File");
        label2.setLayoutX(900);
        label2.setLayoutY(300);
        label2.setFont(ft);
        label2.setTextFill(Color.BLACK); // White text

        Button arrowButton = new Button("⬆");
        arrowButton.setStyle("-fx-background-color: #d1dbe4; -fx-text-fill: white;"); // Aqua background, white text
        arrowButton.setFont(ft);
        arrowButton.setLayoutX(980);
        arrowButton.setLayoutY(375);
        arrowButton.setShape(JI_arrow);

          DropShadow JI_shadowadd = new DropShadow();
          arrowButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                arrowButton.setEffect(JI_shadowadd);
            }
        });

        arrowButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                arrowButton.setEffect(null);
            }
        });
        arrowButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent Event) {
                System.out.println("Add Your file Here");
            }
        });

        // Study Material Button
        Button studyMaterialButton = new Button("Study Material");
        studyMaterialButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: white;"); 
        studyMaterialButton.setLayoutX(100);
        studyMaterialButton.setLayoutY(900);
        studyMaterialButton.setFont(ft);

        studyMaterialButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent Event) {
                System.out.println("Study Material");
            }
        });

        // Assignment Button
        Button assignmentButton = new Button("Assignment");
        assignmentButton.setStyle("-fx-background-color: #7593af; -fx-text-fill: white;"); // SteelBlue background, white text
        assignmentButton.setLayoutX(400);
        assignmentButton.setLayoutY(900);
        assignmentButton.setFont(ft);

        assignmentButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent Event) {
                System.out.println("Assignment Button");
            }
        });

        // Student Button
        Button studentsButton = new Button("Students");
        studentsButton.setStyle("-fx-background-color: #a3b7ca; -fx-text-fill: white;"); // LimeGreen background, white text
        studentsButton.setLayoutX(650);
        studentsButton.setLayoutY(900);
        studentsButton.setFont(ft);

        studentsButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent Event) {
                System.out.println("Student Button");
            }
        });

        // Messages Button
        Button messagesButton = new Button("Messages");
        messagesButton.setStyle("-fx-background-color: #d1dbe4; -fx-text-fill: black;"); // Gold background, black text
        messagesButton.setLayoutX(900);
        messagesButton.setLayoutY(900);
        messagesButton.setFont(ft);

        messagesButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent Event) {
                System.out.println("Message Button");
            }
        });

        VBox topVBox = new VBox(10, label2, arrowButton);
        topVBox.setAlignment(Pos.CENTER);
        topVBox.setPadding(new Insets(20));

        HBox bottomHBox = new HBox(10, studyMaterialButton, assignmentButton, studentsButton, messagesButton);
        bottomHBox.setAlignment(Pos.CENTER);
        bottomHBox.setPadding(new Insets(20));

         // Load the background image
         Image backgroundImage = new Image("images/colorkit (4).png");
         BackgroundImage background = new BackgroundImage(
             backgroundImage,
             BackgroundRepeat.NO_REPEAT,
             BackgroundRepeat.NO_REPEAT,
             BackgroundPosition.CENTER,
             BackgroundSize.DEFAULT
         );


        BorderPane root = new BorderPane();
        //  root.setStyle("-fx-background-color: AQUA;");
        root.setBackground(new Background(background));
        root.setTop(JI_pane);
        root.setCenter(topVBox);
        root.setBottom(bottomHBox);


        // Add the label to a VBox
        VBox vb = new VBox(label1);
        vb.setAlignment(Pos.TOP_CENTER);
        vb.setPadding(new Insets(10));

        // Add the VBox to the pane
        JI_pane.getChildren().addAll(vb);

        // Create the Scene with the Pane
        Scene sc = new Scene(root, 700, 500);
        // Set the Scene to the Stage
        sc_7Stage.setScene(sc);
        sc_7Stage.show();
    }

    

    
}